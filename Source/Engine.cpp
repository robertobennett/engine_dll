#include "stdafx.h"
#include "Engine.h"
#include "Util/ObjReader.h"

CEngine::CEngine() :
m_pRenderer(nullptr),
m_bInitialized(false)
{
	return;
}

CEngine::~CEngine()
{
	delete m_pRenderer;
	m_pRenderer = nullptr;

	m_bInitialized = false;

	return;
}

bool CEngine::Init(HWND hRenderTarget, UINT iRenderTargetWidth, UINT iRenderTargetHeight)
{
	if (!m_bInitialized && hRenderTarget != NULL)
	{
		m_hRenderTarget = hRenderTarget;

		m_pRenderer = new CRenderer();
		if (!m_pRenderer->Init(m_hRenderTarget, iRenderTargetWidth, iRenderTargetHeight))
			return false;

		return true;
	}

	return false;
}

void CEngine::Frame()
{
	m_pRenderer->PreDraw();
	m_pRenderer->Draw();
	m_pRenderer->PostDraw();

	return;
}

void CEngine::Resize(UINT iRenderTargetWidth, UINT iRenderTargetHeight)
{
	if (m_pRenderer != nullptr)
		m_pRenderer->Resize(iRenderTargetWidth, iRenderTargetHeight);

	return;
}