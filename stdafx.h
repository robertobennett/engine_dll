#pragma once
#ifndef ENGINE_DLL_STDAFX_H
#define ENGINE_DLL_STDAFX_H

#ifdef _WINDLL
	#define DLL_IMPORT_EXPORT __declspec(dllexport)
#else
	#define DLL_IMPORT_EXPORT __declspec(dllimport)
#endif

#include <vector>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <fstream>

#include <Windows.h>

#include <d3d11.h>
#include <dxgi.h>
#include <DirectXMath.h>

#endif