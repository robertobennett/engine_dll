#pragma once
#ifndef ENGINE_DLL_ENGINE_H
#define ENGINE_DLL_ENGINE_H

#include <Renderer.h>

class CEngine
{
	public:
		DLL_IMPORT_EXPORT CEngine();
		DLL_IMPORT_EXPORT ~CEngine();
		
		DLL_IMPORT_EXPORT bool Init(HWND hRenderTarget, UINT iRenderTargetWidth, UINT iRenderTargetHeight);
		DLL_IMPORT_EXPORT void Frame();
		DLL_IMPORT_EXPORT void Resize(UINT iRenderTargetWidth, UINT iRenderTargetHeight);

	private:
		HWND m_hRenderTarget;
		bool m_bInitialized;

		CRenderer * m_pRenderer;
};

#endif